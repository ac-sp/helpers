<div class="auth-info" style="padding-top:5px;">
    <ul class="pull-right">
        <?php if (\acsp\shield\Core::getMe()->getAttr('controlLevel') !== \HiMax\Core::CONTROL_NONE && !empty(\acsp\helpers\Auth::getUserData())): ?>
        <li><img class="icon" src="<?= base_url('public/img/user.png'); ?>" alt="ACSP" /><a>Logado como <?= \acsp\helpers\Auth::getUserData()['nome']; ?></a></li>
        <br>
        <li><a style="color:#bc0000;text-decoration: underline;" href="<?= \acsp\helpers\Url::ambienteUrl(ACSP_URL_PAINEL_FRONT); ?>">Painel</a></li>
        <li><a style="color:#bc0000;text-decoration: underline;" href="<?= base_url() . '?logout=1'; ?>">Sair</a></li>
        <?php endif; ?>
    </ul>
</div>