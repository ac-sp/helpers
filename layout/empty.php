<?php
$this->load->helper('url');
$baseUrl = base_url();
?>
<!DOCTYPE HTML>
<html lang="pt-BR" dir="ltr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta charset="UTF-8">
        <meta name="robots" content="noindex,nofollow">
        <title>ACSP</title>
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/font-awesome/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/jquery-ui/themes/base/all.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/skiver/skiver.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/dhtmlx/dhtmlx.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/dhtmlx/skins/terrace/dhtmlx.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!--<link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/DataTables/datatables.min.css">-->
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/DataTables/DataTables-1.10.8/css/jquery.dataTables.min.css">
        <!--<link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>public/css/font-awesome/css/font-awesome.min.css">-->
        <!--<link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>public/css/style.css">-->
        <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>bower_components/front-helpers/css/layout.css">
        <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>bower_components/front-helpers/css/acsp.custom.css">
        <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>public/css/custom.css">
        <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery" src="<?= $baseUrl ?>bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?= $baseUrl ?>bower_components/requirejs/require.js"></script>
        <script type="text/javascript" src="<?= $baseUrl ?>bower_components/front-helpers/js/config-require.js" id="config-require" data-baseurl="<?= base_url('') ?>"></script>
    </head>
    <body class="empty">
        <section id="content" class="container-fluid">
            <?= $content ?>
        </section>
    </body>
</html>