<?php

namespace acsp\helpers\core;

/* Controller Basis */

trait CBasis {
    protected $cbasisAttrDefaults = [
        'layout' => '',
        'errorMsgs' => [
            'notFound' => 'Cadastro não encontrado.',
            'save' => 'Ocorreu um erro interno. Tente novamente.',
            'saveDenied' => 'Permissão negada para alteração deste cadastro.',
            'delete' => 'Não é possível remover esse item.',
        ],
        'infoMsgs' => [
            'created' => 'Registro cadastrado com sucesso',
            'updated' => 'Registro atualizado com sucesso',
        ]
    ];

    public function getAttrProperty() {
        $defaults = array_merge($this->attrDefaults, $this->cbasisAttrDefaults);
        return $defaults;
    }

    public function CB__construct($loadModel = true) {
        parent::__construct();

        $this->layoutDir = @$this->config->config['layoutDir'] ? $this->config->config['layoutDir'] : ('../../' . \acsp\helpers\Url::dir() . '/layout/');
        $this->layout = $this->layoutDir . 'index';

        $loadModel && ($this->Model = $this->loadModel(!property_exists($this, 'modelPath') ? get_class($this) . '_model' : $this->modelPath));

        $this->load->helper('url');
        $this->load->library('session');
    }

    public function loadModel($model) {
        if(!empty($model)) { 
            $modelObj = null;
            $this->load->database();
            $util = !empty($this->config->config['utils']) ? $this->config->config['utils']() : (new \acsp\helpers\Util);
            if(strpos($model, '\\')===false && strpos($model, '_model')!==false) { // custom models created locally into models folder
                if(!class_exists('CI_Model', false)) {// forces codeigniter to load model extensible files
                    try {$this->load->model('XXX_model');}catch(\Exception $e){}
                }
                
                $modelShortName = substr($model, 0, strpos($model, '_model'));
                $modelPath = APPPATH . "models/$model.php";
                if(file_exists($modelPath)) {
                    require_once($modelPath);
                    if(method_exists($model, 'setConnConfig')) { // models that extend Dashes component (DBAL Model)
                        $modelObj = $util::loadDashesModel($model);
                    } else if (is_subclass_of($model, 'CI_Model')) { // models that extend CodeBlaze component (CI Model)
                        $this->load->model($model);
                        $modelObj = $this->{$model};
                    } else { // models that not extend ci_model
                        $modelObj = new $model;
                    }
                }
            } else { // models that are stored directly on composer
                $modelShortName = \Crush\Basic::getClassShortName($model);
                $modelObj = $util::loadDashesModel($model);
            }
            
            $this->{$modelShortName} = $modelObj;
            return $modelObj;
        }
    }

    protected function renderLayout($layout, $data = []) {
        $logininfo = empty($this->logininfo) ? $this->layoutDir . 'logininfo' : $this->logininfo;
        $data['logininfo'] = $this->load->view($logininfo, array(), true);
        $data['infomenu'] = $this->load->view($this->layoutDir . 'infomenu', array(), true);
        $data['topmenu'] = $this->load->view($this->layoutDir . 'topmenu', array(), true);
        $data['sidemenu'] = $this->load->view($this->layoutDir . 'sidemenu', array(), true);

        return $this->load->view($layout, $data, true);
    }

    public function template($template) {
        $this->renderPartial(((string) $this->router->directory) . $this->router->fetch_class() . '/' . $template . '.tpl.html');
    }

    public function sendJsonMsg($data) {
        $json = [];
        foreach(['status', 'load', 'results'] as $key) {
            array_key_exists($key, $data) && ($json[$key] = $data[$key]);
        }
        $json['popmsg'] = [];
        $json['popmsg']['type'] = $data['msgType'];
        $json['popmsg']['text'] = $data['msgText'];

        print(json_encode($json));
    }

}
