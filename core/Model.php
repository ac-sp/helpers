<?php

namespace acsp\helpers\core;

trait Model {
    
    use \acsp\helpers\core\Model_field_format,
        \acsp\helpers\core\LoadModel;
    
    protected $deactivate = \DB_FIELD_DELETE;
    
    public function buildRecordControlShareList($id=NULL) {
        $recordControlLists = [];
        
        $shareList = [];
        foreach(($this->getRecordControlShareList()) as $x => $y) {
            $shareList[] = ['id'=>$x, 'text'=>str_replace(['owner', 'group', 'public', 'user', 'department'], ['Proprietário', 'Perfil', 'Qualquer um', 'Usuário', 'Departamento'], $y)];
        }
        $accessList = [];
        foreach(($this->getRecordControlAccessList()) as $x => $y) {
            $accessList[] = ['id'=>$x,'text'=>str_replace(['read', 'write', 'execute'], ['Leitura', 'Escrita', 'Acesso Web'], $y)];
        }
        $profileList = !empty(\HiMax\Core::getMe()->getModels()['profile']) ? \Crush\Collection::transform(\HiMax\Core::getMe()->getModels()['profile']->findActive([], NULL, NULL, NULL, NULL, -1), '', ['id','name text']) : [];
        $departmentList = !empty(\HiMax\Core::getMe()->getModels()['department']) ? \Crush\Collection::transform(\HiMax\Core::getMe()->getModels()['department']->find([], NULL, NULL, NULL, NULL, -1), '', ['id', 'name text']) : [];
        $userList = !empty(\HiMax\Core::getMe()->getModels()['user']) ? \Crush\Collection::transform(\HiMax\Core::getMe()->getModels()['user']->getListByACL([], ['user', 'name'], ['module'=>'admin', 'class'=>'Page', 'action'=>'index']), '', ['user id','name text']) : [];

        list($recordControlLists['share'], $recordControlLists['access'], $recordControlLists['user'], $recordControlLists['profile'], $recordControlLists['department']) = 
            [$shareList, $accessList, $userList, $profileList, $departmentList];
        
        
        $defaultLists = ['site'=>[],'record'=>[],];
        foreach($this->getRecordControlDefaultLists()['site'] as $x => $y) {
            $defaultLists['site'][] = ['id'=>$x, 'text'=>str_replace(['guest', 'logged',], ['Visitante', 'Usuário autenticado',], $x)];
        }
        foreach($this->getRecordControlDefaultLists()['record'] as $x => $y) {
            $defaultLists['record'][] = ['id'=>$x, 'text'=>str_replace(['public', 'private','my_profile','my_department'], ['Qualquer um', 'Somente eu','Qualquer um do meu grupo','Qualquer um do meu departamento'], $x)];
        }
        
        $recordControlRows = $this->getRecordControlRows($id);
        
        $lists = [];
        $lists['recordControlLists'] = $recordControlLists;
        $lists['recordControlRows'] = $recordControlRows;
        $lists['recordControlDefaults'] = $defaultLists;
        $lists['canManageRow'] = $this->canManageRow($id);
        
        return $lists;
    }

}
